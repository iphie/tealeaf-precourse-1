# merge.rb 

# version 1 of merge method
# Original hash retains its values after the merge.

hashA = {"a" => 10, "b" => 25}
hashB = {"b" => 50, "c" => 60}
merge_a_b = hashA.merge(hashB)

puts "Original Hashes - merge()"
puts hashA
puts hashB
puts "Running hashA.merge(hashB)"
puts merge_a_b
puts "Merge has not destroyed the original hashes"
puts hashA
puts hashB
puts ""



# version 2 of merge! method
# Original values over-written after the merge.

hashC = {"d" => 200, "e" => 350}
hashD = {"e" => 500, "f" => 600}
merge_c_d = hashC.merge!(hashD)

puts "Original Hashes - merge!()"
puts hashC
puts hashD
puts "Running hashC.merge!(hashD)"
puts merge_c_d
puts "Merge has destroyed hashC as it was merged into hashD"
puts hashC
puts hashD