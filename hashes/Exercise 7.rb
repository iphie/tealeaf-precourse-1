#Exercise 7

x = "hi there"
my_hash = {x: "some value"}
my_hash2 = {x => "some value"}

puts my_hash
puts my_hash2

#The first value used "x" as the key whereas the second used "hi there" as the key.