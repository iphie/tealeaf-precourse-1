#anagrams.rb

words =  ['demo', 'none', 'tied', 'evil', 'dome', 'mode', 'live',
          'fowl', 'veil', 'wolf', 'diet', 'vile', 'edit', 'tide',
          'flow', 'neon']

# Read through the words array and put the letters of the word 
# 	in alphabetical order.  If the word is new create a new hash, if it
# 	exists, then append the original word to the existing hash of the
# 	alphabetized word.

# Print the hash arrays as lists of anagrammed words.

result = {}

words.each do |word| 
	key = word.split('').sort.join

# 	if # Word already exists as hash
# 		# Append array value to hash

	if result.has_key?(key)
		result[key].push(word)

	else
		 result[key] = [word]
# 	else # Word does not exist as hash
# 		# Add word as hash to result

 	end

end

# puts result.each {|key, value| puts "#{key}: #{value}"}

result.each do |k, v|
	puts "--------"
	p v
end