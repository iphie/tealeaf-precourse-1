#has_number.rb - Arrays Exercise 1

arr = [1, 3, 5, 7, 9, 11]

number = 3


if arr.include?(number)
	puts "#{number} is included in the array!"
else
	puts "#{number} is not included in the array, oh noes!"
end

