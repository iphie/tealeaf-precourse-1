# number_range.rb

def range(int)
	if int < 0
		puts "Must be a positive integer"
	elsif int < 50 && int >= 0
		puts "The number is between 0 and 50"
	elsif int == 50
		puts "The number is 50"
	elsif int <= 100 && int > 50
		puts "The number is between 50 and 100"
	else
		puts "The number is greater than 100"
	end
end

# Flow Control Exercise 5 - number_range_case.rb 

def range_case(int)
	response = case 
		when int < 0
			"Must be a positive integer"
		when int < 50 && int >= 0
			"The number is between 0 and 50"
		when int == 50
			"The number is 50"
		when int <= 100 && int > 50
			"The number is between 50 and 100"
		else
			"The number is greater than 100" 
	end
		puts response
end


range(-1)
range(25)
range(50)
range(100)
range(1000)

range_case(-1)
range_case(25)
range_case(50)
range_case(100)
range_case(1000)