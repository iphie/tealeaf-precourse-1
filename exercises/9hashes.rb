# 9hashes.rb

h = {a:1, b:2, c:3, d:4}

puts h[:b]

h.store("e", 5)
puts h

h.keep_if{|k, v| v > 3.5}
puts h