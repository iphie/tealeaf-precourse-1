Loops & Iterations - Exercise 1

x = [1, 2, 3, 4, 5]
x.each do |a|
	a + 1
end

The file responds with itself because nothing is being done with the array itself.
