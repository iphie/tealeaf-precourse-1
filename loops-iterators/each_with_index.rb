#each_with_index.rb

array = ["cat", "dog", "wombat", "turtle", "roadkill"]

array.each_with_index do | item, index |
	puts "#{index + 1}. #{item}"
end